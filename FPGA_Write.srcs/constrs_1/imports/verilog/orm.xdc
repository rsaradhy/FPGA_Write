#==================================================================
# Constraints for oRM, using: XC7K70T-1FBG676C
#==================================================================
# VCCO is 3.3V, VCCAUXIO is 1.8V
# Micron N25Q128A13ESE40F, 128 Mbit FLASH, mode "001".
# 40 MHz clk40 is AC-coupled LVDS from NB6N11SMNG
# MGTREFCLK is AC-coupled from Si5324C-C-GM
#==================================================================
#Ported to XDC by Rohith (Manaully)



# NET “clk_p” LOC = AD12    -> set_property LOC AD12 [get_ports clk_p]

set_property LOC N21 [get_ports clk40_p]
set_property IOSTANDARD LVDS_25 [get_ports clk40_p]
set_property DIFF_TERM false [get_ports clk40_p]


set_property LOC N22 [get_ports clk40_n]
set_property IOSTANDARD LVDS_25 [get_ports clk40_n]
set_property DIFF_TERM false [get_ports clk40_n]


################################################

set_property LOC B15 [get_ports rx_p0]
set_property IOSTANDARD LVDS_25 [get_ports rx_p0]
set_property DIFF_TERM false [get_ports rx_p0]

set_property LOC A15 [get_ports rx_n0]
set_property IOSTANDARD LVDS_25 [get_ports rx_n0]
set_property DIFF_TERM false [get_ports rx_n0]

set_property LOC A13 [get_ports rx_p1]
set_property IOSTANDARD LVDS_25 [get_ports rx_p1]
set_property DIFF_TERM false [get_ports rx_p1]

set_property LOC A12 [get_ports rx_n1]
set_property IOSTANDARD LVDS_25 [get_ports rx_n1]
set_property DIFF_TERM false [get_ports rx_n1]

set_property LOC R22 [get_ports rx_p2]
set_property IOSTANDARD LVDS_25 [get_ports rx_p2]
set_property DIFF_TERM false [get_ports rx_p2]

set_property LOC R23 [get_ports rx_n2]
set_property IOSTANDARD LVDS_25 [get_ports rx_n2]
set_property DIFF_TERM false [get_ports rx_n2]



#####################################################

set_property LOC C14 [get_ports tx_p0]
set_property IOSTANDARD TMDS_33 [get_ports tx_p0]

set_property LOC C13 [get_ports tx_n0]
set_property IOSTANDARD TMDS_33 [get_ports tx_n0]

set_property LOC K23 [get_ports tx_p1]
set_property IOSTANDARD TMDS_33 [get_ports tx_p1]

set_property LOC J23 [get_ports tx_n1]
set_property IOSTANDARD TMDS_33 [get_ports tx_n1]


######################################################
set_property LOC R26 [get_ports tx_p2]
set_property IOSTANDARD TMDS_33 [get_ports tx_p2]


#set_property LOC R26 [get_ports tx_p2]
#set_property IOSTANDARD LVCMOS33 [get_ports tx_p2]
#set_property DIFF_TERM false [get_ports tx_p2]
#set_property SLEW FAST [get_ports tx_p2]
#set_property DRIVE 12 [get_ports tx_p2]

set_property LOC P26 [get_ports tx_n2]
set_property IOSTANDARD TMDS_33 [get_ports tx_n2]

#set_property LOC P26 [get_ports tx_n2]
#set_property IOSTANDARD LVCMOS33 [get_ports tx_n2]
#set_property DIFF_TERM false [get_ports tx_n2]
#set_property SLEW FAST [get_ports tx_n2]
#set_property DRIVE 12 [get_ports tx_n2]

##########################################################

set_property LOC E26 [get_ports dat0]
set_property IOSTANDARD LVCMOS33 [get_ports dat0]
set_property SLEW FAST [get_ports dat0]
set_property DRIVE 12 [get_ports dat0]

set_property LOC D26 [get_ports dat1]
set_property IOSTANDARD LVCMOS33 [get_ports dat1]
set_property SLEW FAST [get_ports dat1]
set_property DRIVE 12 [get_ports dat1]

set_property LOC E25 [get_ports rdy0]
set_property IOSTANDARD LVCMOS33 [get_ports rdy0]
set_property SLEW FAST [get_ports rdy0]
set_property DRIVE 12 [get_ports rdy0]

set_property LOC D25 [get_ports rdy1]
set_property IOSTANDARD LVCMOS33 [get_ports rdy1]
set_property SLEW FAST [get_ports rdy1]
set_property DRIVE 12 [get_ports rdy1]

##########################################################

set_property LOC G25 [get_ports led]
set_property IOSTANDARD LVCMOS33 [get_ports led]
set_property SLEW SLOW [get_ports led]
set_property DRIVE 12 [get_ports led]

#########################################################

set_property LOC H24 [get_ports sw0]
set_property IOSTANDARD LVCMOS33 [get_ports sw0]
set_property PULLUP true [get_ports sw0]

set_property LOC J26 [get_ports sw1]
set_property IOSTANDARD LVCMOS33 [get_ports sw1]
set_property PULLUP true [get_ports sw1]

set_property LOC M25 [get_ports sw2]
set_property IOSTANDARD LVCMOS33 [get_ports sw2]
set_property PULLUP true [get_ports sw2]

set_property LOC N26 [get_ports sw3]
set_property IOSTANDARD LVCMOS33 [get_ports sw3]
set_property PULLUP true [get_ports sw3]

###########################################################

set_property LOC N23 [get_ports spi_miso]
set_property IOSTANDARD LVCMOS33 [get_ports spi_miso]
set_property SLEW SLOW [get_ports spi_miso]
set_property DRIVE 12 [get_ports spi_miso]

set_property LOC R20 [get_ports spi_mosi]
set_property IOSTANDARD LVCMOS33 [get_ports spi_mosi]
set_property PULLUP true [get_ports spi_mosi]

set_property LOC R21 [get_ports spi_sck]
set_property IOSTANDARD LVCMOS33 [get_ports spi_sck]
set_property PULLUP true [get_ports spi_sck]

set_property LOC L23 [get_ports spi_cs0]
set_property IOSTANDARD LVCMOS33 [get_ports spi_cs0]
set_property PULLUP true [get_ports spi_cs0]

set_property LOC M22 [get_ports spi_cs1]
set_property IOSTANDARD LVCMOS33 [get_ports spi_cs1]
set_property PULLUP true [get_ports spi_cs1]

#########################################################

set_property LOC C23 [get_ports FCS_B]
set_property IOSTANDARD LVCMOS33 [get_ports FCS_B]
set_property SLEW FAST [get_ports FCS_B]
set_property DRIVE 12 [get_ports FCS_B]

set_property LOC B24 [get_ports DQ0]
set_property IOSTANDARD LVCMOS33 [get_ports DQ0]
set_property SLEW FAST [get_ports DQ0]
set_property DRIVE 12 [get_ports DQ0]

set_property LOC A25 [get_ports DQ1]
set_property IOSTANDARD LVCMOS33 [get_ports DQ1]
set_property PULLUP true [get_ports DQ1]

#########################################################


set_property LOC A22 [get_ports led1]
set_property IOSTANDARD LVCMOS33 [get_ports led1]
set_property SLEW FAST [get_ports led1]
set_property DRIVE 12 [get_ports led1]

set_property LOC A23 [get_ports led2]
set_property IOSTANDARD LVCMOS33 [get_ports led2]
set_property SLEW FAST [get_ports led2]
set_property DRIVE 12 [get_ports led2]

#########################################################

create_clock -name clk40_p -period 25 -waveform {0 12.5} [get_ports clk40_p]

#########################################################

create_clock -name spi_sck -period 30 -waveform {0 15} [get_ports spi_sck]

#########################################################
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets rx_p0]

set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets rx_p1]

set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets rx_p2]
#########################################################




