
 
 
 

 



window new WaveWindow  -name  "Waves for BMG Example Design"
waveform  using  "Waves for BMG Example Design"


      waveform add -signals /si5324_conf_tb/status
      waveform add -signals /si5324_conf_tb/si5324_conf_synth_inst/bmg_port/CLKA
      waveform add -signals /si5324_conf_tb/si5324_conf_synth_inst/bmg_port/ADDRA
      waveform add -signals /si5324_conf_tb/si5324_conf_synth_inst/bmg_port/DOUTA
console submit -using simulator -wait no "run"
