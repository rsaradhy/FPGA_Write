`timescale 1ns / 1ps

module clock_init
  (input clk40_in,
   output clk40_out,
   output clk80_out,
   output clk160_out,
   output clk320_out,
   output pll_locked,
   output reset_out,
   output osc_out,
   input cclk_in,
   input cclk_cs);

   wire clk40_buf;
   BUFG buf_clk40 (.I(clk40_in),.O(clk40_buf));

   wire pwrdwn = 1'b0;

   // Briefly issue a reset_pll at power-up.
   reg [15:0] counter;
   wire reset_pll;
   always @ (posedge clk40_buf)
     if (counter[15] == 1'b0) counter <= counter + 1'b1;
     else counter <= counter;
   assign reset_pll = ~counter[15];

   // Create the 40 MHz clock and multiples.
   wire clkfbout0, clkfbout_buf0;
   wire clkout0, clkout1, clkout2, clkout3;
   
   PLLE2_BASE #
     (.BANDWIDTH("OPTIMIZED"),
      .CLKFBOUT_MULT(32),
      .CLKFBOUT_PHASE(0.000),
      .CLKIN1_PERIOD(25.000),
      .CLKOUT0_DIVIDE(32), // 40 MHz
      .CLKOUT0_PHASE(0.0),
      .CLKOUT0_DUTY_CYCLE(0.5),
      .CLKOUT1_DIVIDE(16), // 80 MHz
      .CLKOUT1_PHASE(0.0),
      .CLKOUT1_DUTY_CYCLE(0.5),
      .CLKOUT2_DIVIDE(8), // 160 MHz
      .CLKOUT2_PHASE(0.0),
      .CLKOUT2_DUTY_CYCLE(0.5),
      .CLKOUT3_DIVIDE(4), // 320 MHz
      .CLKOUT3_PHASE(0.0),
      .CLKOUT3_DUTY_CYCLE(0.5),
      .DIVCLK_DIVIDE(1),
      .REF_JITTER1(0.010),
      .STARTUP_WAIT("FALSE"))
   PLLE2_BASE_i1 
     (.CLKOUT0(clkout0),
      .CLKOUT1(clkout1),
      .CLKOUT2(clkout2),
      .CLKOUT3(clkout3),
      .CLKFBOUT(clkfbout0),
      .LOCKED(pll_locked),
      .CLKIN1(clk40_buf),
      .PWRDWN(pwrdwn),
      .RST(reset_pll),
      .CLKFBIN(clkfbout_buf0));

   BUFG buf_fb1(.I(clkfbout0),.O(clkfbout_buf0));
   BUFG buf_40 (.I(clkout0),.O(clk40_out));
   BUFG buf_80 (.I(clkout1),.O(clk80_out));
   BUFG buf_160(.I(clkout2),.O(clk160_out));
   BUFG buf_320(.I(clkout3),.O(clk320_out));
   
   //----------------------------------------------------------------
   // Instantiate the on-chip oscillator.
   //----------------------------------------------------------------

   wire zero, one;
   assign zero = 1'b0;
   assign one  = 1'b1;
   
   STARTUPE2 STARTUPE2_i
     (.CFGMCLK(osc_out),
      .CLK(zero),
      .GSR(zero),
      .GTS(zero),
      .KEYCLEARB(zero),
      .PACK(zero),
      .USRCCLKO(cclk_in),
      .USRCCLKTS(cclk_cs),
      .USRDONEO(one),
      .USRDONETS(one));

   // Briefly issue a reset at power-up.
   reg [23:0] counter_osc;
   always @ (posedge osc_out)
     if (counter_osc[23] == 1'b0) counter_osc <= counter_osc + 1'b1;
     else counter_osc <= counter_osc;
   assign reset_out = ~counter_osc[23];
   
endmodule
