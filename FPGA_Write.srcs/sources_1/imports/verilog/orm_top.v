
`timescale 1ns / 1ps

module orm_top
  (input clk40_p, clk40_n, // lvds

   input rx_p0, rx_n0, // lvds
   input rx_p1, rx_n1, // lvds
   input rx_p2, rx_n2, // lvds

   output tx_p0, tx_n0, // tmds
   output tx_p1, tx_n1, // tmds
   output tx_p2, tx_n2, // tmds

   output dat0, dat1, rdy0, rdy1,
   input sw0, sw1, sw2, sw3,
   
   input spi_mosi, spi_sck, spi_cs0, spi_cs1,
   output spi_miso,

   output FCS_B,// FLASH CS
   output DQ0,  // FPGA2MEM
   input DQ1,   // MEM2FPGA
   
   inout si5324_sda, // REFCLK generator
   inout si5324_scl, // REFCLK generator
   
   // GTXE2 Channel X0Y3 :: Dubbed Channel 0
   output sfp_tx_p0, sfp_tx_n0,
   input sfp_rx_p0, sfp_rx_n0,
   input refclk_p0, refclk_n0,
   output sfp_tx_disable_0,
   
   
   // GTXE2 Channel X0Y0 :: Dubbed Channel 1
   output sfp_tx_p1, sfp_tx_n1,
   input sfp_rx_p1, sfp_rx_n1,
   input refclk_p1, refclk_n1,
   output sfp_tx_disable_1,
   
   input sfp_los0, // I have no idea why this is here
   

   // led outputs...
   output led,
   output led1,
   output led2
   
   );
   
   //----------------------------------------------------------------------
   // Define the peripheral addresses.
   //----------------------------------------------------------------------
   `include "ORM_ADDR.v"
   //----------------------------------------------------------------------
   
   //==============================================================
   // Tri-state the SPI output. 
   // The SPI interfaces will fail without this.
   //==============================================================
   
   wire spi_miso0, spi_miso1;
   wire tmp_miso, enable_miso;
   assign tmp_miso = ((~spi_cs0) & spi_miso0) | ((~spi_cs1) & spi_miso1);
   assign enable_miso = ~((~spi_cs0) | (~spi_cs1));
   OBUFT OBUFT_i (.O(spi_miso), .I(tmp_miso), .T(enable_miso));

   //==============================================================
   // declarations
   //==============================================================

   wire zero = 1'b0;
   wire one = 1'b1;
   
   // clock_init
   wire clk40raw, reset_por;
   wire clk40, clk80, clk160, clk320;
   wire osc, pll_locked;
   wire spi_gclk, cclk_in, cclk_cs;

   // spi_interface
   wire [15:0] spi_ctl[1:0];
   wire [15:0] spi_stat[1:0];
   wire [15:0] peripheral2pi, peripheral2pi_all;
   wire [15:0] pi2peripheral;
   wire peripheral_read, peripheral_write;
   wire [9:0] peripheral_addr;

   //==============================================================
   // Reset.
   //==============================================================
   
   reg reset40;
   always @(posedge clk40) reset40 <= reset_por;

   //==============================================================
   // Instantiate LVDS input buffers.
   //==============================================================

   IBUFDS IBUFDS_clk40 (.O(clk40raw), .I(clk40_p), .IB(clk40_n));

   wire rx0, rx1, rx2;
   IBUFDS IBUFDS_rx0 (.O(rx0), .I(rx_p0), .IB(rx_n0));
   IBUFDS IBUFDS_rx1 (.O(rx1), .I(rx_p1), .IB(rx_n1));
   IBUFDS IBUFDS_rx2 (.O(rx2), .I(rx_p2), .IB(rx_n2));

   //==============================================================
   // Instantiate TMDS output buffers.
   //==============================================================

   wire tx0, tx1, tx2;

   assign tx0 = clk40;
   assign tx1 = clk40;
   
   OBUFDS OBUFDS_tx0 (.O(tx_p0), .OB(tx_n0), .I(tx0));
   OBUFDS OBUFDS_tx1 (.O(tx_p1), .OB(tx_n1), .I(tx1));
   OBUFDS OBUFDS_tx2 (.O(tx_p2), .OB(tx_n2), .I(tx2)); //tx2 is user accessible in the board
   
   //==============================================================
   // LEDs
   //==============================================================
   wire led_wire,led1_wire,led2_wire;
   assign led = led_wire;
   assign led1 = led1_wire;
   assign led2 = led2_wire;
   
   // Creating a second pulse using the 40MHz Clock...
   Second_Pulse one_second_pulse(
      .clock_in(clk40) ,.sig_out(led2_wire) );



   
   //==============================================================
   // Create the clocks.
   //==============================================================

   BUFG buf_sck (.I(spi_sck),.O(spi_gclk));

   clock_init clock_init_i
     (.clk40_in(clk40raw),
      .clk40_out(clk40),
      .clk80_out(clk80),
      .clk160_out(clk160),
      .clk320_out(clk320),
      .pll_locked(pll_locked),
      .reset_out(reset_por),
      .osc_out(osc),
      .cclk_in(cclk_in),
      .cclk_cs(cclk_cs));
   

      
   //==============================================================
   // SPI interface.
   //==============================================================
   // CS0 is the normal runtime interface.
   // CS1 is the bridge to the Xilinx SPI FLASH memory.
   //==============================================================
   // control register at address 0x000
   // control register at address 0x001
   // status registerc at address 0x080
   // status register  at address 0x081
   // peripherals at addresses 0x100->0x3FF
   //==============================================================
   
   assign spi_stat[0] = 16'hAA55;
   assign spi_stat[1] = 16'hAA55;
   
   spiInterface spiInterface_i
     (.reset(reset_por),
      .mosi(spi_mosi),
      .miso(spi_miso0),
      .cs(spi_cs0),
      .spiclk(spi_gclk),
      .controlReg0(spi_ctl[0]),
      .controlReg1(spi_ctl[1]),
      .statusReg0(spi_stat[0]),
      .statusReg1(spi_stat[1]),
      .peripheral2pi(peripheral2pi_all),
      .pi2peripheral(pi2peripheral),
      .peripheral_write(peripheral_write),
      .peripheral_read(peripheral_read),
      .peripheral_addr(peripheral_addr));

   //==============================================================
   // Reprogram the FLASH via SPI interface using CS1.
   //==============================================================

   assign FCS_B = spi_cs1; // FLASH CS
   assign DQ0 = spi_mosi;  // FPGA2MEM
   assign spi_miso1 = DQ1; // MEM2FPGA
   assign cclk_in = spi_gclk;
   assign cclk_cs = spi_cs1;

   //==============================================================
   // Housekeeping registers.
   //==============================================================

   // firmware version
   wire [15:0] firmware_version;
   wire [3:0] RSRVD_MSB, RSRVD_LSB;
   wire [3:0] major_version, minor_version;
   assign RSRVD_MSB = 4'h0;
   assign RSRVD_LSB = 4'h0;
   assign major_version = 4'h1;
   assign minor_version = 4'h0;
   assign firmware_version = {RSRVD_MSB, major_version, 
			      RSRVD_LSB, minor_version};
   
   reg [15:0] dummy_reg0, dummy_reg1;
   wire [15:0] constant0, constant1;
   assign constant0 = 16'h1001;
   assign constant1 = 16'hBEEF;

   always @(posedge clk40)
     if (reset40 == 1'b1) dummy_reg0 <= 16'h0;
     else if ((peripheral_addr == DATA_DUMMY_REG0) && 
	      (peripheral_write == 1'b1)) dummy_reg0 <= pi2peripheral;
     else dummy_reg0 <= dummy_reg0;
   
   always @(posedge clk40)
     if (reset40 == 1'b1) dummy_reg1 <= 16'h0;     
     else if ((peripheral_addr == DATA_DUMMY_REG1) && 
	      (peripheral_write == 1'b1)) dummy_reg1 <= pi2peripheral;
     else dummy_reg1 <= dummy_reg1;
   
   assign peripheral2pi_all 
     = peripheral2pi |
       (peripheral_addr == DATA_FIRMWARE_VERSION ? firmware_version : 16'h0) |
       (peripheral_addr == DATA_CONSTANT0 ? constant0 : 16'h0) |
       (peripheral_addr == DATA_CONSTANT1 ? constant1 : 16'h0) |
       (peripheral_addr == DATA_DUMMY_REG0 ? dummy_reg0 : 16'h0) |
       (peripheral_addr == DATA_DUMMY_REG1 ? dummy_reg1 : 16'h0);
   
   //==============================================================
   // SPI peripheral
   //==============================================================


   //==============================================================
   // CPU interface
   //==============================================================

   assign dat0 = zero;
   assign dat1 = zero;
   assign rdy0 = zero;
   assign rdy1 = zero;
   
   
   //==============================================================
   // Set up Si5324 to create a 160 MHz clock from 40MHz input clock (check the coe file)
   //==============================================================

   wire si5324_done;
   si5324 si5324_cfg(.RESET(reset_por),.CLK(osc),
                     .SDA(si5324_sda),.SCL(si5324_scl),.DONE(si5324_done));



    
   //==============================================================
   // Create the SFP block.
   //==============================================================
    
    //Enable the sfp by setting the disable pin to zero
   assign  sfp_tx_disable_0 = zero; // enabling the sfps
   assign sfp_tx_disable_1 = zero; // enabling the sfps
   
   //setting the tx data value to be FF00 for hte
   wire [15:0] data_value;
   assign data_value = 16'hFF00;
   
   //creating a new reset for GTX
   reg reset_gtx;
      always @(posedge clk40) reset_gtx <= reset_por;
   
   //GTX Wizard wrapper...
   gtwizard_0 GTX(
     .soft_reset_tx_in(reset_gtx),
     .soft_reset_rx_in(reset_gtx),
     .dont_reset_on_data_error_in(one),
     .q0_clk1_gtrefclk_pad_n_in(refclk_n0),
     .q0_clk1_gtrefclk_pad_p_in(refclk_p0),
   //  .gt0_tx_fsm_reset_done_out(gt0_tx_fsm_reset_done_out),
   //  .gt0_rx_fsm_reset_done_out(gt0_rx_fsm_reset_done_out),
     .gt0_data_valid_in(one),
   //  .gt1_tx_fsm_reset_done_out(gt1_tx_fsm_reset_done_out),
   //  .gt1_rx_fsm_reset_done_out(gt1_rx_fsm_reset_done_out),
     .gt1_data_valid_in(one),
   
   // .gt0_txusrclk_out(gt0_txusrclk_out),
   // .gt0_txusrclk2_out(gt0_txusrclk2_out),
   // .gt0_rxusrclk_out(gt0_rxusrclk_out),
   // .gt0_rxusrclk2_out(gt0_rxusrclk2_out),
   
   // .gt1_txusrclk_out(gt1_txusrclk_out),
   // .gt1_txusrclk2_out(gt1_txusrclk2_out),
   // .gt1_rxusrclk_out(gt1_rxusrclk_out),
   // .gt1_rxusrclk2_out(gt1_rxusrclk2_out),
    //_________________________________________________________________________
    //GT0  (X1Y0)
    //____________________________CHANNEL PORTS________________________________
    //------------------------------- CPLL Ports -------------------------------
   //     .gt0_cpllfbclklost_out          (gt0_cpllfbclklost_out), // output wire gt0_cpllfbclklost_out
   //     .gt0_cplllock_out               (gt0_cplllock_out), // output wire gt0_cplllock_out
        .gt0_cpllreset_in               (reset_gtx), // input wire gt0_cpllreset_in
    //-------------------------- Channel - DRP Ports  --------------------------
        .gt0_drpaddr_in                 (9'h0), // input wire [8:0] gt0_drpaddr_in
        .gt0_drpdi_in                   (16'h0), // input wire [15:0] gt0_drpdi_in
   //     .gt0_drpdo_out                  (gt0_drpdo_out), // output wire [15:0] gt0_drpdo_out
        .gt0_drpen_in                   (zero), // input wire gt0_drpen_in
   //     .gt0_drprdy_out                 (gt0_drprdy_out), // output wire gt0_drprdy_out
        .gt0_drpwe_in                   (zero), // input wire gt0_drpwe_in
    //------------------------- Digital Monitor Ports --------------------------
   //     .gt0_dmonitorout_out            (gt0_dmonitorout_out), // output wire [7:0] gt0_dmonitorout_out
    //------------------- RX Initialization and Reset Ports --------------------
        .gt0_eyescanreset_in            (reset_gtx), // input wire gt0_eyescanreset_in
        .gt0_rxuserrdy_in               (one), // input wire gt0_rxuserrdy_in
    //------------------------ RX Margin Analysis Ports ------------------------
   //     .gt0_eyescandataerror_out       (gt0_eyescandataerror_out), // output wire gt0_eyescandataerror_out
        .gt0_eyescantrigger_in          (zero), // input wire gt0_eyescantrigger_in
   
   
    //---------------- Receive Ports - FPGA RX interface Ports -----------------
   //     .gt0_rxdata_out                 (gt0_rxdata_out), // output wire [15:0] gt0_rxdata_out
    
    
    //------------------------- Receive Ports - RX AFE -------------------------
        .gt0_gtxrxp_in                  (sfp_rx_p0), // input wire gt0_gtxrxp_in
        .gt0_gtxrxn_in                  (sfp_rx_n0), // input wire gt0_gtxrxn_in
    
    //------------------- Receive Ports - RX Equalizer Ports -------------------
        .gt0_rxdfelpmreset_in           (reset_gtx), // input wire gt0_rxdfelpmreset_in
   //     .gt0_rxmonitorout_out           (gt0_rxmonitorout_out), // output wire [6:0] gt0_rxmonitorout_out
        .gt0_rxmonitorsel_in            (2'h1), // input wire [1:0] gt0_rxmonitorsel_in
    //------------- Receive Ports - RX Fabric Output Control Ports -------------
   //     .gt0_rxoutclkfabric_out         (gt0_rxoutclkfabric_out), // output wire gt0_rxoutclkfabric_out
    //----------- Receive Ports - RX Initialization and Reset Ports ------------
        .gt0_gtrxreset_in               (reset_gtx), // input wire gt0_gtrxreset_in
        .gt0_rxpmareset_in              (reset_gtx), // input wire gt0_rxpmareset_in
    //------------ Receive Ports -RX Initialization and Reset Ports ------------
   //     .gt0_rxresetdone_out            (gt0_rxresetdone_out), // output wire gt0_rxresetdone_out
    //------------------- TX Initialization and Reset Ports --------------------
        .gt0_gttxreset_in               (reset_gtx), // input wire gt0_gttxreset_in
        .gt0_txuserrdy_in               (one), // input wire gt0_txuserrdy_in
    //---------------- Transmit Ports - TX Data Path interface -----------------
        .gt0_txdata_in                  (data_value), // input wire [15:0] gt0_txdata_in
    //-------------- Transmit Ports - TX Driver and OOB signaling --------------
        .gt0_gtxtxn_out                 (sfp_tx_n0), // output wire gt0_gtxtxn_out
        .gt0_gtxtxp_out                 (sfp_tx_p0), // output wire gt0_gtxtxp_out
    //--------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
   //     .gt0_txoutclkfabric_out         (gt0_txoutclkfabric_out), // output wire gt0_txoutclkfabric_out
   //     .gt0_txoutclkpcs_out            (gt0_txoutclkpcs_out), // output wire gt0_txoutclkpcs_out
    //----------- Transmit Ports - TX Initialization and Reset Ports -----------
   //     .gt0_txresetdone_out            (gt0_txresetdone_out), // output wire gt0_txresetdone_out
   
    //GT1  (X1Y3)
    //____________________________CHANNEL PORTS________________________________
    //------------------------------- CPLL Ports -------------------------------
   //     .gt1_cpllfbclklost_out          (gt1_cpllfbclklost_out), // output wire gt1_cpllfbclklost_out
   //     .gt1_cplllock_out               (gt1_cplllock_out), // output wire gt1_cplllock_out
        .gt1_cpllreset_in               (reset_gtx), // input wire gt1_cpllreset_in
    //-------------------------- Channel - DRP Ports  --------------------------
        .gt1_drpaddr_in                 (9'h0), // input wire [8:0] gt1_drpaddr_in
        .gt1_drpdi_in                   (16'h0), // input wire [15:0] gt1_drpdi_in
   //     .gt1_drpdo_out                  (gt1_drpdo_out), // output wire [15:0] gt1_drpdo_out
        .gt1_drpen_in                   (zero), // input wire gt1_drpen_in
   //     .gt1_drprdy_out                 (gt1_drprdy_out), // output wire gt1_drprdy_out
        .gt1_drpwe_in                   (zero), // input wire gt1_drpwe_in
    //------------------------- Digital Monitor Ports --------------------------
   //     .gt1_dmonitorout_out            (gt1_dmonitorout_out), // output wire [7:0] gt1_dmonitorout_out
    //------------------- RX Initialization and Reset Ports --------------------
        .gt1_eyescanreset_in            (reset_gtx), // input wire gt1_eyescanreset_in
        .gt1_rxuserrdy_in               (one), // input wire gt1_rxuserrdy_in
    //------------------------ RX Margin Analysis Ports ------------------------
   //     .gt1_eyescandataerror_out       (gt1_eyescandataerror_out), // output wire gt1_eyescandataerror_out
        .gt1_eyescantrigger_in          (zero), // input wire gt1_eyescantrigger_in
    
    
    //---------------- Receive Ports - FPGA RX interface Ports -----------------
   //     .gt1_rxdata_out                 (gt1_rxdata_out), // output wire [15:0] gt1_rxdata_out
        
        
        
        
    //------------------------- Receive Ports - RX AFE -------------------------
        .gt1_gtxrxp_in                  (sfp_rx_p1), // input wire gt1_gtxrxp_in
        .gt1_gtxrxn_in                  (sfp_rx_n1), // input wire gt1_gtxrxn_in
    
    
    
   //------------------- Receive Ports - RX Equalizer Ports -------------------
        .gt1_rxdfelpmreset_in           (reset_gtx), // input wire gt1_rxdfelpmreset_in
   //     .gt1_rxmonitorout_out           (gt1_rxmonitorout_out), // output wire [6:0] gt1_rxmonitorout_out
        .gt1_rxmonitorsel_in            (2'h1), // input wire [1:0] gt1_rxmonitorsel_in
    //------------- Receive Ports - RX Fabric Output Control Ports -------------
   //     .gt1_rxoutclkfabric_out         (gt1_rxoutclkfabric_out), // output wire gt1_rxoutclkfabric_out
    //----------- Receive Ports - RX Initialization and Reset Ports ------------
        .gt1_gtrxreset_in               (reset_gtx), // input wire gt1_gtrxreset_in
        .gt1_rxpmareset_in              (reset_gtx), // input wire gt1_rxpmareset_in
    //------------ Receive Ports -RX Initialization and Reset Ports ------------
   //     .gt1_rxresetdone_out            (gt1_rxresetdone_out), // output wire gt1_rxresetdone_out
    //------------------- TX Initialization and Reset Ports --------------------
        .gt1_gttxreset_in               (reset_gtx), // input wire gt1_gttxreset_in
        .gt1_txuserrdy_in               (one), // input wire gt1_txuserrdy_in
        
        
        
    //---------------- Transmit Ports - TX Data Path interface -----------------
        .gt1_txdata_in                  (data_value), // input wire [15:0] gt1_txdata_in
        
        
        
    //-------------- Transmit Ports - TX Driver and OOB signaling --------------
        .gt1_gtxtxn_out                 (sfp_tx_n1), // output wire gt1_gtxtxn_out
        .gt1_gtxtxp_out                 (sfp_tx_p1), // output wire gt1_gtxtxp_out
    //--------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
   //     .gt1_txoutclkfabric_out         (gt1_txoutclkfabric_out), // output wire gt1_txoutclkfabric_out
   //     .gt1_txoutclkpcs_out            (gt1_txoutclkpcs_out), // output wire gt1_txoutclkpcs_out
    //----------- Transmit Ports - TX Initialization and Reset Ports -----------
   //     .gt1_txresetdone_out            (gt1_txresetdone_out), // output wire gt1_txresetdone_out
   
    //____________________________COMMON PORTS________________________________
   // .gt0_qplloutclk_out(gt0_qplloutclk_out),
   // .gt0_qplloutrefclk_out(gt0_qplloutrefclk_out),
     .sysclk_in(clk40) //What is this used for?
   
   );



   
   
endmodule