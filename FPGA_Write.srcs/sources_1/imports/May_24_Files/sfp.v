`timescale 1ns / 1ps

module sfp
  (input clk,
   input reset,
   output tx_init_done,
   input tx_valid,
   output tx_clk,
   input [15:0] data_tx,
   output rx_clk,
   output [15:0] data_rx,
   input refclk_p, refclk_n,
   input sfp_rx_p, sfp_rx_n,
   output sfp_tx_p, sfp_tx_n);

   reg resetq;
   always @(posedge clk) resetq <= reset;

   wire zero, one;
   assign zero = 1'b0;
   assign one = 1'b1;
   
   for_SFP for_SFP_0
     (
//      .gt0_tx_fsm_reset_done_out(tx_init_done),
      .gt0_txresetdone_out(tx_init_done),
      .soft_reset_tx_in(resetq),
      .soft_reset_rx_in(resetq),
      .dont_reset_on_data_error_in(1'b1),
      .q0_clk1_gtrefclk_pad_n_in(refclk_n),
      .q0_clk1_gtrefclk_pad_p_in(refclk_p),
      //.gt0_tx_fsm_reset_done_out(),
      //.gt0_rx_fsm_reset_done_out(),
      .gt0_data_valid_in(tx_valid),
      //.gt0_txusrclk_out(),
      .gt0_txusrclk2_out(tx_clk),
      //.gt0_rxusrclk_out(),
      .gt0_rxusrclk2_out(rx_clk),
      
      //_________________________________________________________________________
      //GT0  (X1Y0)
      //____________________________CHANNEL PORTS________________________________
      
      //------------------------------- CPLL Ports -------------------------------
      //.gt0_cpllfbclklost_out(),
      //.gt0_cplllock_out(),
      .gt0_cpllreset_in(resetq),

      //-------------------------- Channel - DRP Ports  --------------------------
      .gt0_drpaddr_in(9'h0), // [8:0]
      .gt0_drpdi_in(16'h0), // [15:0]
      //.gt0_drpdo_out(), // [15:0]
      .gt0_drpen_in(1'b0),
      //.gt0_drprdy_out(),
      .gt0_drpwe_in(1'b0),

      //------------------------- Digital Monitor Ports --------------------------
      //.gt0_dmonitorout_out(), // [7:0]

      //------------------- RX Initialization and Reset Ports --------------------
      .gt0_eyescanreset_in(resetq),
      .gt0_rxuserrdy_in(1'b1),

      //------------------------ RX Margin Analysis Ports ------------------------
      //.gt0_eyescandataerror_out(),
      .gt0_eyescantrigger_in(1'b0),

      //---------------- Receive Ports - FPGA RX interface Ports -----------------
      .gt0_rxdata_out(data_rx), // [15:0]

      //------------------------- Receive Ports -------------------------
      .gt0_gtxrxp_in(sfp_rx_p),
      .gt0_gtxrxn_in(sfp_rx_n),

      //------------------- Receive Ports - RX Equalizer Ports -------------------
      .gt0_rxdfelpmreset_in(resetq),
      //.gt0_rxmonitorout_out(), // [6:0]
      .gt0_rxmonitorsel_in(2'h1), // [1:0]

      //------------- Receive Ports - RX Fabric Output Control Ports -------------
      //.gt0_rxoutclkfabric_out(),

      //----------- Receive Ports - RX Initialization and Reset Ports ------------
      .gt0_gtrxreset_in(resetq),
      .gt0_rxpmareset_in(resetq),

      //------------ Receive Ports -RX Initialization and Reset Ports ------------
      //.gt0_rxresetdone_out(),

      //------------------- TX Initialization and Reset Ports --------------------
      .gt0_gttxreset_in(resetq),
      .gt0_txuserrdy_in(1'b1),

      //---------------- Transmit Ports - TX Data Path interface -----------------
      .gt0_txdata_in(data_tx), // [15:0]

      //-------------- Transmit Ports - TX Driver --------------
      .gt0_gtxtxn_out(sfp_tx_n),
      .gt0_gtxtxp_out(sfp_tx_p),

      //--------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
      //.gt0_txoutclkfabric_out(),
      //.gt0_txoutclkpcs_out(),

      //----------- Transmit Ports - TX Initialization and Reset Ports -----------
      //.gt0_txresetdone_out(),
      
      //____________________________COMMON PORTS________________________________
      .gt0_qplloutclk_out(),
      .gt0_qplloutrefclk_out(),
      .sysclk_in(clk));
   
endmodule
