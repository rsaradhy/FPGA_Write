#HEADER
# Date: Thursday, June 21, 2018 5:36 PM
# File Version: 3
# Software Name: Precision Clock EVB Software
# Software Version: 5.1
# Software Date: July 23, 2014
# Part number: Si5324
#END_HEADER
#PROFILE
# Name: Si5324
#INPUT
# Name: CKIN
# Channel: 1
# Frequency (MHz): 40.000000
# N3: 20
# Maximum (MHz): 40.000000
# Minimum (MHz): 37.890625
#END_INPUT
#PLL
# Name: PLL
# Frequency (MHz): 5120.000000
# f3 (MHz): 2.000000
# N1_HS: 4
# N2_HS: 10
# N2_LS: 256
# Phase Offset Resolution (ns): 0.78125
# BWSEL_REG Option: Frequency (Hz)
# 10:    9
#  9:   17
#  8:   35
#  7:   70
#  6:  140
#  5:  285
#  4:  589
#END_PLL
#OUTPUT
# Name: CKOUT
# Channel: 1
# Frequency (MHz): 160.000000
# NC1_LS: 8
# CKOUT1 to CKIN1 Ratio: 4 / 1
# Maximum (MHz): 160.000000
# Minimum (MHz): 151.562500
#END_OUTPUT
#OUTPUT
# Name: CKOUT
# Channel: 2
# Frequency (MHz): 160.000000
# NC_LS: 8
# CKOUT2 to CKOUT1 Ratio: 1 / 1
# Maximum (MHz): 160.000000
# Minimum (MHz): 151.562500
#END_OUTPUT
#CONTROL_FIELD
# Register-based Controls
#        FREE_RUN_EN: 0x0
#    CKOUT_ALWAYS_ON: 0x0
#         BYPASS_REG: 0x0
#          CK_PRIOR2: 0x1
#          CK_PRIOR1: 0x0
#          CKSEL_REG: 0x0
#              DHOLD: 0x0
#            SQ_ICAL: 0x1
#          BWSEL_REG: 0x4
#        AUTOSEL_REG: 0x0
#           HIST_DEL: 0x14
#              ICMOS: 0x3
#              SLEEP: 0x0
#         SFOUT2_REG: 0x7
#         SFOUT1_REG: 0x7
#          FOSREFSEL: 0x2
#             HLOG_2: 0x0
#             HLOG_1: 0x0
#           HIST_AVG: 0x1A
#          DSBL2_REG: 0x0
#          DSBL1_REG: 0x0
#             PD_CK2: 0x1
#             PD_CK1: 0x0
#         FLAT_VALID: 0x1
#             FOS_EN: 0x0
#            FOS_THR: 0x1
#            VALTIME: 0x2
#              LOCKT: 0x0
#        CK2_BAD_PIN: 0x1
#        CK1_BAD_PIN: 0x1
#            LOL_PIN: 0x1
#            INT_PIN: 0x0
#         INCDEC_PIN: 0x1
#       CK1_ACTV_PIN: 0x1
#          CKSEL_PIN: 0x0
#        CK_ACTV_POL: 0x1
#         CK_BAD_POL: 0x1
#            LOL_POL: 0x1
#            INT_POL: 0x1
#           LOS2_MSK: 0x1
#           LOS1_MSK: 0x1
#           LOSX_MSK: 0x1
#           FOS2_MSK: 0x1
#           FOS1_MSK: 0x1
#            LOL_MSK: 0x1
#              N1_HS: 0x0
#             NC1_LS: 0x7
#             NC2_LS: 0x7
#              N2_LS: 0xFF
#              N2_HS: 0x6
#                N31: 0x13
#                N32: 0x13
#         CLKIN2RATE: 0x0
#         CLKIN1RATE: 0x0
#           FASTLOCK: 0x0
#            LOS1_EN: 0x3
#            LOS2_EN: 0x3
#            FOS1_EN: 0x1
#            FOS2_EN: 0x1
#   INDEPENDENTSKEW1: 0x0
#   INDEPENDENTSKEW2: 0x0
#END_CONTROL_FIELD
#REGISTER_MAP
  0, 14h
  1, E4h
  2, 42h
  3, 15h
  4, 14h
  5, EDh
  6, 3Fh
  7, 2Ah
  8, 00h
  9, D0h
 10, 00h
 11, 42h
 19, 30h
 20, 3Eh
 21, FEh
 22, DFh
 23, 1Fh
 24, 3Fh
 25, 00h
 31, 00h
 32, 00h
 33, 07h
 34, 00h
 35, 00h
 36, 07h
 40, C0h
 41, 00h
 42, FFh
 43, 00h
 44, 00h
 45, 13h
 46, 00h
 47, 00h
 48, 13h
 55, 00h
131, 18h
132, 00h
137, 00h
138, 0Fh
139, FFh
142, 00h
143, 00h
136, 40h
#END_REGISTER_MAP
#END_PROFILE
