onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib si5324_conf_opt

do {wave.do}

view wave
view structure
view signals

do {si5324_conf.udo}

run -all

quit -force
