vlib work
vlib riviera

vlib riviera/xil_defaultlib
vlib riviera/xpm

vmap xil_defaultlib riviera/xil_defaultlib
vmap xpm riviera/xpm

vlog -work xil_defaultlib  -sv2k12 \
"/home/rsaradhy/Software/Vivado/Vivado/2018.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -93 \
"/home/rsaradhy/Software/Vivado/Vivado/2018.1/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib  -v2k5 \
"../../../ip/for_SFP0/for_sfp0_common_reset.v" \
"../../../ip/for_SFP0/for_sfp0_common.v" \
"../../../ip/for_SFP0/for_sfp0_gt_usrclk_source.v" \
"../../../ip/for_SFP0/for_sfp0_support.v" \
"../../../ip/for_SFP0/for_SFP0/example_design/for_sfp0_tx_startup_fsm.v" \
"../../../ip/for_SFP0/for_SFP0/example_design/for_sfp0_rx_startup_fsm.v" \
"../../../ip/for_SFP0/for_sfp0_init.v" \
"../../../ip/for_SFP0/for_sfp0_gt.v" \
"../../../ip/for_SFP0/for_sfp0_multi_gt.v" \
"../../../ip/for_SFP0/for_SFP0/example_design/for_sfp0_sync_block.v" \
"../../../ip/for_SFP0/for_sfp0.v" \

vlog -work xil_defaultlib \
"glbl.v"

