-makelib ies_lib/xil_defaultlib -sv \
  "/home/rsaradhy/Software/Vivado/Vivado/2018.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \
-endlib
-makelib ies_lib/xpm \
  "/home/rsaradhy/Software/Vivado/Vivado/2018.1/data/ip/xpm/xpm_VCOMP.vhd" \
-endlib
-makelib ies_lib/xil_defaultlib \
  "../../../ip/for_SFP0/for_sfp0_common_reset.v" \
  "../../../ip/for_SFP0/for_sfp0_common.v" \
  "../../../ip/for_SFP0/for_sfp0_gt_usrclk_source.v" \
  "../../../ip/for_SFP0/for_sfp0_support.v" \
  "../../../ip/for_SFP0/for_SFP0/example_design/for_sfp0_tx_startup_fsm.v" \
  "../../../ip/for_SFP0/for_SFP0/example_design/for_sfp0_rx_startup_fsm.v" \
  "../../../ip/for_SFP0/for_sfp0_init.v" \
  "../../../ip/for_SFP0/for_sfp0_gt.v" \
  "../../../ip/for_SFP0/for_sfp0_multi_gt.v" \
  "../../../ip/for_SFP0/for_SFP0/example_design/for_sfp0_sync_block.v" \
  "../../../ip/for_SFP0/for_sfp0.v" \
-endlib
-makelib ies_lib/xil_defaultlib \
  glbl.v
-endlib

