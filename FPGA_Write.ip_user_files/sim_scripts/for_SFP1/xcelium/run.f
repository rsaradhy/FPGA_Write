-makelib xcelium_lib/xil_defaultlib -sv \
  "/home/rsaradhy/Software/Vivado/Vivado/2018.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \
-endlib
-makelib xcelium_lib/xpm \
  "/home/rsaradhy/Software/Vivado/Vivado/2018.1/data/ip/xpm/xpm_VCOMP.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../ip/for_SFP1/for_sfp1_common_reset.v" \
  "../../../ip/for_SFP1/for_sfp1_common.v" \
  "../../../ip/for_SFP1/for_sfp1_gt_usrclk_source.v" \
  "../../../ip/for_SFP1/for_sfp1_support.v" \
  "../../../ip/for_SFP1/for_SFP1/example_design/for_sfp1_tx_startup_fsm.v" \
  "../../../ip/for_SFP1/for_SFP1/example_design/for_sfp1_rx_startup_fsm.v" \
  "../../../ip/for_SFP1/for_sfp1_init.v" \
  "../../../ip/for_SFP1/for_sfp1_gt.v" \
  "../../../ip/for_SFP1/for_sfp1_multi_gt.v" \
  "../../../ip/for_SFP1/for_SFP1/example_design/for_sfp1_sync_block.v" \
  "../../../ip/for_SFP1/for_sfp1.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  glbl.v
-endlib

