vlib modelsim_lib/work
vlib modelsim_lib/msim

vlib modelsim_lib/msim/xil_defaultlib
vlib modelsim_lib/msim/xpm

vmap xil_defaultlib modelsim_lib/msim/xil_defaultlib
vmap xpm modelsim_lib/msim/xpm

vlog -work xil_defaultlib -64 -incr -sv \
"/home/rsaradhy/Software/Vivado/Vivado/2018.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -64 -93 \
"/home/rsaradhy/Software/Vivado/Vivado/2018.1/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib -64 -incr \
"../../../ip/for_SFP1/for_sfp1_common_reset.v" \
"../../../ip/for_SFP1/for_sfp1_common.v" \
"../../../ip/for_SFP1/for_sfp1_gt_usrclk_source.v" \
"../../../ip/for_SFP1/for_sfp1_support.v" \
"../../../ip/for_SFP1/for_SFP1/example_design/for_sfp1_tx_startup_fsm.v" \
"../../../ip/for_SFP1/for_SFP1/example_design/for_sfp1_rx_startup_fsm.v" \
"../../../ip/for_SFP1/for_sfp1_init.v" \
"../../../ip/for_SFP1/for_sfp1_gt.v" \
"../../../ip/for_SFP1/for_sfp1_multi_gt.v" \
"../../../ip/for_SFP1/for_SFP1/example_design/for_sfp1_sync_block.v" \
"../../../ip/for_SFP1/for_sfp1.v" \

vlog -work xil_defaultlib \
"glbl.v"

