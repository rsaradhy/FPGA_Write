onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib for_SFP1_opt

do {wave.do}

view wave
view structure
view signals

do {for_SFP1.udo}

run -all

quit -force
