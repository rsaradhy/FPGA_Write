#==================================================================
# Constraints for oRM, using: XC7K70T-1FBG676C
#==================================================================
# VCCO is 3.3V, VCCAUXIO is 1.8V
# Micron N25Q128A13ESE40F, 128 Mbit FLASH, mode "001".
# 40 MHz clk40 is AC-coupled LVDS from NB6N11SMNG
# MGTREFCLK is AC-coupled from Si5324C-C-GM
#==================================================================
#Ported to XDC by Rohith (Manaully)



# NET “clk_p” LOC = AD12    -> set_property LOC AD12 [get_ports clk_p]



#########################################################
#SFP Stuff
#########################################################


###---------- Set placement for gt0_gtx_wrapper_i/GTXE2_CHANNEL ------
#set_property LOC GTXE2_CHANNEL_X0Y0 [get_cells for_SFP0_support_i/inst/for_SFP0_init_i/for_SFP0_i/gt0_for_SFP0_i/gtxe2_i]
###---------- Set placement for gt1_gtx_wrapper_i/GTXE2_CHANNEL ------
#set_property LOC GTXE2_CHANNEL_X0Y3 [get_cells for_SFP0_support_i/inst/for_SFP0_init_i/for_SFP0_i/gt1_for_SFP0_i/gtxe2_i]

#set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets sfp_1/for_SFP_0/inst/common0_i/gt0_qplloutclk_out]

# GTXE2 Channel X0Y3 

set_property LOC H2 [get_ports sfp_tx_p0]
set_property LOC H1 [get_ports sfp_tx_n0]

set_property LOC J4 [get_ports sfp_rx_p0]
set_property LOC J3 [get_ports sfp_rx_n0]


set_property LOC K6 [get_ports refclk_p0]
set_property LOC K5 [get_ports refclk_n0]

set_property LOC A18 [get_ports sfp_tx_disable_0]
set_property IOSTANDARD LVCMOS33 [get_ports sfp_tx_disable_0]


# GTXE2 Channel X0Y0
set_property LOC P2 [get_ports sfp_tx_p1]
set_property LOC P1 [get_ports sfp_tx_n1]

set_property LOC R4 [get_ports sfp_rx_p1]
set_property LOC R3 [get_ports sfp_rx_n1]

set_property LOC H6 [get_ports refclk_p1]
set_property LOC H5 [get_ports refclk_n1]


set_property LOC F14 [get_ports sfp_tx_disable_1]
set_property IOSTANDARD LVCMOS33 [get_ports sfp_tx_disable_1]



# Stuff for the 125MHz Clock
set_property LOC T19 [get_ports si5324_sda]
set_property IOSTANDARD LVCMOS33 [get_ports si5324_sda]

set_property LOC T18 [get_ports si5324_scl]
set_property IOSTANDARD LVCMOS33 [get_ports si5324_scl]

set_property LOC D15 [get_ports sfp_los0]
set_property IOSTANDARD LVCMOS33 [get_ports sfp_los0]




#########################################################
