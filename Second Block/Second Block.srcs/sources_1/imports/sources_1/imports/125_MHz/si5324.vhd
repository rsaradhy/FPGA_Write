----------------------------------------------------------------------------------
-- Engineer: Marcelo Vicente
-- Module Name: si5324_boot - Behavioral
-- Project Name: Optical Receiver Mezzanine
-- Target Devices: xc7k70tfbg676-1
-- Tool Versions: Vivado 2014.2
-- Description: Si5324 boot controller
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity si5324 is
    Port ( RESET : in STD_LOGIC;
           CLK : in STD_LOGIC;
           SDA : inout STD_LOGIC;
           SCL : inout STD_LOGIC;
           DONE : out STD_LOGIC);
end si5324;

architecture Behavioral of si5324 is 
	signal resetn : std_logic;
	signal i2c_en : std_logic;
	signal i2c_rw : std_logic;
	signal i2c_data_in : std_logic_vector (7 downto 0);
	signal i2c_busy : std_logic;
	signal i2c_data_out : std_logic_vector (7 downto 0);
	signal i2c_error : std_logic;
	
	signal i2c_data_in_mux : std_logic;
	
	signal ram_data : std_logic_vector (7 downto 0);
	
	signal ram_addr : std_logic_vector (7 downto 0);
	signal ram_addr_inc : std_logic;
	signal ram_addr_clr : std_logic;
	
	signal counter : std_logic_vector (31 downto 0) := (others => '0');
	signal counter_en : std_logic;
	signal counter_clr : std_logic;
	
	type states is (st_idle, st_wait10ms, st_i2c_write_addr, st_i2c_write_data, st_i2c_wait, st_i2c_next, st_checklock_write, st_checklock_read1, st_checklock_read2, st_checklock_read3, st_checklock_check, st_done);
	signal state : states := st_idle;
	
	attribute mark_debug : string;
	attribute mark_debug of i2c_en, i2c_rw, i2c_data_in, i2c_busy, i2c_data_out, i2c_error, ram_addr, state : signal is "true";
begin

	i2c_controller : entity work.i2c_master
	PORT MAP(
		clk => CLK,
		reset_n => resetn,
		ena => i2c_en,
		addr =>"1101000",
		rw => i2c_rw,
		data_wr => i2c_data_in,
		busy => i2c_busy,
		data_rd => i2c_data_out,
		ack_error => i2c_error,
		sda => SDA,
		scl => SCL
	);
	resetn <= not RESET;
	i2c_data_in <= ram_data when i2c_data_in_mux = '0' else X"82";

	si5324_regs : entity work.si5324_conf
	PORT MAP (
		clka => CLK,
		addra => ram_addr,
		douta => ram_data
	);

	-- RAM address
	process (CLK) begin
		if CLK'event and CLK = '1' then
			if ram_addr_clr = '1' then
				ram_addr <= (others => '0');
			elsif ram_addr_inc = '1' then
				ram_addr <= ram_addr + 1;
			end if;
		end if;
	end process;
	
	-- Counter for delays
	process (CLK) begin
		if CLK'event and CLK = '1' then
			if counter_clr = '1' then
				counter <= (others => '0');
			elsif counter_en = '1' then
				counter <= counter + 1;
			end if;
		end if;
	end process;

	process (RESET, CLK) begin
		if RESET = '1' then
			state <= st_idle;
			DONE <= '0';
			i2c_en <= '0';
			i2c_rw <= '0';
			i2c_data_in_mux <= '0';
			ram_addr_inc <= '0';
			ram_addr_clr <= '1';
			counter_en <= '0';
			counter_clr <= '1';
		elsif CLK'event and CLK = '1' then
			DONE <= '0';
			i2c_en <= '0';
			i2c_rw <= '0';
			i2c_data_in_mux <= '0';
			ram_addr_inc <= '0';
			ram_addr_clr <= '0';
			counter_en <= '0';
			counter_clr <= '0';
			
			case (state) is
				when st_idle =>
					if i2c_busy = '0' then
						state <= st_wait10ms;
					end if;
					ram_addr_clr <= '1';
					counter_clr <= '1';
					
				when st_wait10ms =>
					counter_en <= '1';
					if counter = 400800 then
						state <= st_i2c_write_addr;
					end if;
					
				when st_i2c_write_addr =>
					i2c_en <= '1';
					if i2c_busy = '1' then
						ram_addr_inc <= '1';
						state <= st_i2c_write_data;
					end if;
					
				when st_i2c_write_data =>
					i2c_en <= '1';
					if i2c_busy = '0' then
						state <= st_i2c_wait;
					end if;
				
				when st_i2c_wait =>
					i2c_en <= '1';
					if i2c_busy = '1' then
						ram_addr_inc <= '1';
						state <= st_i2c_next;
					end if;
				
				when st_i2c_next =>
					if i2c_busy = '0' then
						if ram_data = X"FF" or ram_addr = X"00" then
							state <= st_checklock_write;
						else
							state <= st_i2c_write_addr;
						end if;
					end if;
				
				when st_checklock_write =>
					i2c_en <= '1';
					i2c_data_in_mux <= '1';
					if i2c_busy = '1' then
						state <= st_checklock_read1;
					end if;
				when st_checklock_read1 =>
					i2c_en <= '1';
					i2c_rw <= '1';
					if i2c_busy = '0' then
						state <= st_checklock_read2;
					end if;
					
				when st_checklock_read2 =>
					i2c_en <= '1';
					i2c_rw <= '1';
					if i2c_busy = '1' then
						state <= st_checklock_read3;
					end if;
					
				when st_checklock_read3 =>
					if i2c_busy = '0' then
						state <= st_checklock_check;
					end if;
					
				when st_checklock_check =>
					--if i2c_data_out(0) = '0' and i2c_data_out(6) = '1' then
					if i2c_data_out(0) = '0' then
						state <= st_done;
					else
						state <= st_checklock_write;
					end if;
					
				when st_done =>
					DONE <= '1';
					
				when others =>
					state <= st_idle;
			end case;
		end if;
	end process;

end Behavioral;
