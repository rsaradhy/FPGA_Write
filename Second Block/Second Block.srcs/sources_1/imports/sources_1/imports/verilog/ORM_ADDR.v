
localparam DATA_FIRMWARE_VERSION = (10'h101); // firmware_version[15:0];
localparam DATA_CONSTANT0        = (10'h102); // constant0[15:0]; // 0xDEAD
localparam DATA_CONSTANT1        = (10'h103); // constant1[15:0]; // 0xBEEF
localparam DATA_DUMMY_REG0       = (10'h104); // dummy_reg0[15:0]; // writable, but does nothing
localparam DATA_DUMMY_REG1       = (10'h105); // dummy_reg1[15:0]; // writable, but does nothing
