`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/23/2018 02:32:54 PM
// Design Name: 
// Module Name: 125_Clock_Generator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module 125_Clock_Generator(
output 125_MHz_CLK
    );
    
    wire osc, reset;
    
       assign cclk_in = spi_gclk;
       assign cclk_cs = spi_cs1;
    
       //----------------------------------------------------------------
       // Instantiate the on-chip oscillator.
       //----------------------------------------------------------------
    
       wire zero, one;
       assign zero = 1'b0;
       assign one  = 1'b1;
    
       STARTUPE2 STARTUPE2_i
         (.CFGMCLK(osc),
          .CLK(zero),
          .GSR(zero),
          .GTS(zero),
          .KEYCLEARB(zero),
          .PACK(zero),
          .USRCCLKO(cclk_in),
          .USRCCLKTS(cclk_cs),
          .USRDONEO(one),
          .USRDONETS(one));
    
    
       // Briefly issue a reset at power-up.
       reg [23:0] counter_osc;
       always @ (posedge osc)
         if (counter_osc[23] == 1'b0) counter_osc <= counter_osc + 1'b1;
         else counter_osc <= counter_osc;
       assign reset = ~counter_osc[23];
    
    
       // Set up Si5324 to create a free-running 125 MHz clock for the ethernet.
       wire si5324_done;
       si5324 si5324_cfg(.RESET(reset),.CLK(osc),
                         .SDA(si5324_sda),.SCL(si5324_scl),.DONE(si5324_done));
    
    
    
    
endmodule
