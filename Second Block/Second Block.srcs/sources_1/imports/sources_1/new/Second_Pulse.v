`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/22/2018 04:03:27 PM
// Design Name: 
// Module Name: Second_Pulse
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Second_Pulse(
    input clock_in,
    output sig_out
    );
    
    parameter clk_speed = 40000000;
    reg[31:0] counter = 0;
    
    
    reg out = 1'b1;
    
    assign sig_out = out;
    
    always @(posedge clock_in) 
            begin
                if(counter==clk_speed - 1)
                begin
                counter<=0;
                out<=!out;
                end
                else counter<= counter + 1;
            end
            
     
endmodule
